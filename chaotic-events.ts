import { JsonValidator } from "./json-validator";
import AJV from "ajv";
import { compile, JSONSchema } from "json-schema-to-typescript";

let validator = new JsonValidator(new AJV());

const eventSchema: JSONSchema = {
  title: "event schema",
  type: "object",
  properties: {
    id: { type: "string" },
    timestamp: { type: "number" },
    user: {
      type: "object",
      properties: {
        id: { type: "number" },
        image_url: { type: "string" },
        name: { type: "string" },
        username: { type: "string" },
      },
      required: ["id", "image_url", "name", "username"],
      additionalProperties: false,
    },
    message: { type: "string" },
    tags: {
      type: "array",
      items: {
        type: "string",
      },
    },
  },
  required: ["id", "timestamp", "user", "message"],
  additionalProperties: false,
};

export interface EventSchema {
  id: string;
  timestamp: number;
  user: {
    id: number;
    image_url: string;
    name: string;
    username: string;
  };
  message: string;
  tags?: string[];
}

export class ChaoticEvents {
  protected events: EventSchema[];

  constructor() {
    this.events = [];
  }

  async pushEvent(event: EventSchema) {
    console.log(event);

    if (validator.validate(eventSchema, event)) {
      this.events.push(event);
    } else {
      console.log("validation error");
    }
  }

  averageNoOfEventsPerMinute() {
    let average: number = (this.events.length / 10) * 60;
    
    return average;
  }

  wordsFrequency() {
    let frequentWordsAndFrequency: { [k: string]: number } = {};
    let tempEventWordsList: string[];

    this.events.forEach((event) => {
      tempEventWordsList = event.message.split(" ");

      tempEventWordsList.forEach((word) => {
        let sanitaizedWord = word.replace(/[^a-zA-Z0-9']+/g, "");

        if (sanitaizedWord.toLowerCase() in frequentWordsAndFrequency) {
          frequentWordsAndFrequency[sanitaizedWord.toLowerCase()] += 1;
        } else {
          frequentWordsAndFrequency[sanitaizedWord.toLowerCase()] = 1;
        }
      });
    });

    let wordFrequency: [string, number][] = Object.keys(frequentWordsAndFrequency).map((key) => {
      return [key, frequentWordsAndFrequency[key]];
    });

    wordFrequency.sort((first, second): any => {
      if(typeof first[1] === "number" && typeof second[1] === "number") {
        return second[1] - first[1];
      }
    });

    return wordFrequency.slice(0, 10);
  }
}
