import { Ajv } from "ajv";

export class JsonValidator {
  constructor(protected ajv: Ajv) {}

  public validate(schema: object, data: object) {
    return this.ajv.validate(schema, data);
  }
}
