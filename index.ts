import * as WebSocketClient from "websocket";
import { ChaoticEvents } from "./chaotic-events";

let client = new WebSocketClient.w3cwebsocket(
  "wss://chaotic-stream.herokuapp.com/"
);

let chaoticEvents = new ChaoticEvents();
let count = 0;

client.onopen = () => {
  setTimeout(() => {
    client.close();
    console.log("\n\n###############################################################\n\n");
    console.log(
      "Average Number of events per minute:",
      chaoticEvents.averageNoOfEventsPerMinute()
    );
    console.log("\n\n###############################################################\n\n");
    console.log(
      "Top 10 most frequent words and their frequency\n",
      chaoticEvents.wordsFrequency()
    );
    console.log("\n\n###############################################################\n\n");
  }, 10000);

  console.log("WebSocket Client Connected");
};

client.onmessage = (event): void => {
  if (typeof event.data === "string") {
    count = count + 1;

    console.log(count);
    chaoticEvents.pushEvent(JSON.parse(event.data));
  }
};

client.onclose = () => {
  console.log("WebScoket Client Closed");
};
