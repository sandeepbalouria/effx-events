# Instructions to validate results 

git clone https://gitlab.com/sandeepbalouria/effx-events.git
cd effx-events
npm i 
npm run start

# Expectations
The program runs for 10 secnds. Logs the event and count on the console.
It exits after 10 seconds and outputs:-
1. Average number of events in one minute.
2. Top 10 most frequently used words and the word frequency.
